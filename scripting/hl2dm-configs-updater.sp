#include <sourcemod>
#include <steamtools>
#include <updater>

#define UPDATE_URL "http://bitbucket.org/gavvvr/sm-hl2dm-configs-updater/raw/master/hl2dm-configs-updater.txt"

#define BANLSTFILE "banned_hl2dmnet.cfg"

public Plugin:myinfo =
{
	name = "HL2DM Configs Updater",
	author = "Maxtasy, hl2dm.net",
	description = "Automatically updates all (PMS) config files to the latest version.",
	version = "0.2.21",
	url = "https://hl2dm.net"
};

public OnPluginStart()
{
	if (LibraryExists("updater"))
	{
		Updater_AddPlugin(UPDATE_URL)
	}
}

public OnLibraryAdded(const String:name[])
{
	if (StrEqual(name, "updater"))
	{
		Updater_AddPlugin(UPDATE_URL)
	}
}

public Updater_OnPluginUpdated()
{
	ServerCommand("exec %s", BANLSTFILE); //Execute banlist
	ServerCommand("writeid;writeip"); // write them to banned_users and banned_ip
	ReloadPlugin();
}