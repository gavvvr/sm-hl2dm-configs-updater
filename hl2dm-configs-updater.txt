"Updater"
{

	"Information"
	{
		"Version"
		{
			"Latest"	"0.2.21"
			"Previous"	"0.2.20"
		}
		
		"Notes"	"Banlist updated"
	}
	
	"Files"
	{

		"Patch"
		{
			"Plugin"	"Path_SM/plugins/hl2dm-configs-updater.smx"
			"Plugin"	"Path_Mod/cfg/banned_hl2dmnet.cfg"

			"Source"	"Path_SM/scripting/hl2dm-configs-updater.sp"
		}

		"Plugin"	"Path_SM/plugins/hl2dm-configs-updater.smx"
		"Plugin"	"Path_SM/plugins/logconnections.smx"

		"Plugin"	"Path_SM/configs/pms.cfg"

		"Plugin"	"Path_SM/data/mappool/1on1.txt"
		"Plugin"	"Path_SM/data/mappool/2on2.txt"
		"Plugin"	"Path_SM/data/mappool/3on3.txt"

		"Plugin"	"Path_Mod/cfg/autoexec.cfg"
		"Plugin"	"Path_Mod/cfg/banned_hl2dmnet.cfg"
		"Plugin"	"Path_Mod/cfg/pure_server_full.txt"
		"Plugin"	"Path_Mod/cfg/server.cfg"

		"Plugin"	"Path_Mod/cfg/match/1on1.cfg"
		"Plugin"	"Path_Mod/cfg/match/efps1v1.cfg"
		"Plugin"	"Path_Mod/cfg/match/efpsteam.cfg"
		"Plugin"	"Path_Mod/cfg/match/kb_1on1_400.cfg"
		"Plugin"	"Path_Mod/cfg/match/kb_1on1_600.cfg"
		"Plugin"	"Path_Mod/cfg/match/kb_tdm_400.cfg"
		"Plugin"	"Path_Mod/cfg/match/kb_tdm_600.cfg"
		"Plugin"	"Path_Mod/cfg/match/overtime.cfg"
		"Plugin"	"Path_Mod/cfg/match/quicky.cfg"
		"Plugin"	"Path_Mod/cfg/match/rumblecup.cfg"
		"Plugin"	"Path_Mod/cfg/match/shorty.cfg"
		"Plugin"	"Path_Mod/cfg/match/tdm.cfg"

		"Plugin"	"Path_Mod/cfg/misc/countdown.cfg"
		"Plugin"	"Path_Mod/cfg/misc/general_match_settings.cfg"
		"Plugin"	"Path_Mod/cfg/misc/smac_cvars_checker.cfg"
		"Plugin"	"Path_Mod/cfg/misc/sourcetv.cfg"

		"Plugin"	"Path_Mod/cfg/sourcemod/sm_warmode_on.cfg"

		"Source"	"Path_SM/scripting/hl2dm-configs-updater.sp"
		"Source"	"Path_SM/scripting/logconnections.sp"
	}
}